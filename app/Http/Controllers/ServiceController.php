<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

    public function getResourceN(Request $request){
      try {
        $data = [
          "resource_id" => $request->input("resource_id"),
          "token" => $request->input("token"),
          "path" => $request->path(),
          "method" => $request->method(),
          "timestamp" => time()
        ];
        return json_encode($data);
      } catch (\Exception $e) {
        return json_encode(["message" => "Failed to proccess request."]);
      }
    }
}
